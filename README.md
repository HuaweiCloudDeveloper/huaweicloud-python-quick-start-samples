# HuaweiCloud Python 演示代码

## 1. 简介

面向Python开发者提供的从入门到深入了解华为云服务，提供一系列的场景化的解决方案以及示例代码和指导文档，帮助开发者全面的了解云上架构，使用云上资源，实现应用快速上云。

## 2. 项目演示

* [MVC单体应用 -> LibraryManagement](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-python-quick-start-samples/tree/master-dev/LibraryManagement)
* [爬虫应用 -> Spider4Mirror](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-python-quick-start-samples/tree/master-dev/Spider4Mirror)

## 3. 数据库

### 3.1 关系型数据库 RDS

* [MySQL -> RDS4MySQL](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-python-quick-start-samples/tree/master-dev/RDS4MySQL)
* [PostgreSQL -> RDS4PostgreSQL](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-python-quick-start-samples/tree/master-dev/RDS4PostgreSQL)
* [SQLServer -> RDS4SQLServer](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-python-quick-start-samples/tree/master-dev/RDS4SQLServer)

### 3.2 非关系型数据库 NoSQL

* [DDS(兼容MongoDb) -> NoSQL4Mongo](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-python-quick-start-samples/tree/master-dev/NoSQL4Mongo)
* [GaussDB(for Influx) -> NoSQL4Influx](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-python-quick-start-samples/tree/master-dev/NoSQL4Influx)
* [GaussDB(for Cassandra) -> NoSQL4Cassandra](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-python-quick-start-samples/tree/master-dev/NoSQL4Cassandra)

## 4. 应用中间件

### 4.1 分布式缓存服务 DCS

* [Redis -> Cache4Redis](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-python-quick-start-samples/tree/master-dev/Cache4Redis)

### 4.2 分布式消息服务 DMS

* [Kafka -> MessageQueue4Kafka](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-python-quick-start-samples/tree/master-dev/MessageQueue4Kafka)
* [RabbitMQ -> MessageQueue4RabbitMQ](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-python-quick-start-samples/tree/master-dev/MessageQueue4RabbitMQ)
* [RocketMQ -> MessageQueue4RocketMQ](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-python-quick-start-samples/tree/master-dev/MessageQueue4RocketMQ)