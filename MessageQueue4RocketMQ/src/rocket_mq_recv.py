from MessageQueue4RocketMQ.src.rocket_mq_connect import RocketMQConnect


if __name__ == '__main__':
    rocket = RocketMQConnect()
    rocket.start_consume_message()
