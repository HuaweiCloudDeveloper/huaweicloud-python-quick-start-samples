# 华为云 LibraryManagement

## 1. 说明
本项目是一个前后端分离的图书管理系统

### 1.1 环境准备
* Python 3.9
* NodeJS 16.17.0
* PyCharm 2022+
* MySQL 5.7.38

### 1.2 项目架构
* 基于Flask服务的框架 + Vue前端框架 + MySQL数据库（或者 PostgreSQL数据库）
* ORM使用SQLAlchemy包来操作数据库

### 1.3 核心代码介绍
* <kbd>LibraryManagement/flask_backend/app/api/v1/model/book.py</kbd>  
图书数据表模型
```python
class Book(Base):
    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String(50), nullable=False)
    author = Column(String(30), default="未名")
    summary = Column(String(1000))
    image = Column(String(100))
```

* <kbd>LibraryManagement/flask_backend/app/api/v1/book.py</kbd>  
图书的增删改查功能实现


## 2. 快速开始

### 2.1 Flask后端服务启动

#### 2.1.1 安装依赖包
* 进入后端项目根路径 <kbd>LibraryManagement/flask_backend</kbd>
* 安装依赖包 <kbd>pip install -r requirements-dev.txt</kbd>

#### 2.1.2 数据库配置
* 配置<kbd>.development.env</kbd> 文件中的 <kbd>SQLALCHEMY_DATABASE_URI</kbd> 选项
```
# MySQL数据库配置示例: 
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://username:password@ip_address:port/db_name'
```
```
# PostgrepSQL数据库配置示例：
SQLALCHEMY_DATABASE_URI = 'postgresql://username:password@ip_address:port/db_name'
```
> Tips: 记得去掉注释，即<kbd>SQLALCHEMY_DATABASE_URI</kbd>前的 ' # ' 符号；    
> 您所使用的数据库账号必须具有创建数据表的权限，否则将无法为您自动创建数据表  

> ip_address: 数据库IP地址  
> port: 数据库端口  
> username: 用户名   
> password: 用户密码   
> db_name: 数据库名  

#### 2.1.3 服务启动命令

* 在项目根路径运行命令 <kbd>flask db init</kbd>，用来添加超级管理员，默认用户密码为 root, 123456 
```python
# 超级管理员用户密码修改文件路径 LibraryManagement/flask_backend/app/cli/db/init.py
root.username = "username"
root.password = "password"
```
> Tips: 如果您需要一些业务样例数据，可以执行脚本 <kbd>flask db fake</kbd> 添加它
* 在项目根路径运行命令 <kbd>flask run</kbd> 启动后端服务  
![flask_run](image4md/flask_run.png)  
![flask_web](image4md/flask_web.png)   
> 服务默认端口为5000，指定端口方法 <kbd>flask run -p 5000</kbd>  
> 访问 http://localhost:5000 , 出现“Redoc Swagger” 两个API文档超链接  
> 点击“Redoc”，将跳转到Redoc页面；点击“Swagger”，跳转到Swagger页面  
> 这证明您已经成功的将服务运行起来了  


### 2.2 前端服务启动

#### 2.2.1 安装依赖包
* 进入前端项目根路径 <kbd>LibraryManagement/vue_fronted</kbd>
* 安装依赖包 npm install

#### 2.2.2 配置API地址
* 打开配置文件 <kbd>src/config/index.js</kbd>,配置 <kbd>baseUrl</kbd>为对应后端服务url
> 上一步启动的flask服务访问地址：http://localhost:5000

#### 2.2.3 运行
* 本项目是基于 vue-cli3 开发的，默认的本地服务启动的命令为 <kbd>npm run serve</kbd>
> Tips：依赖包出现问题可尝试根据错误信息删除相应依赖包重新安装  
> 遇到 Node Sass 兼容性问题，可尝试下面的命令 <kbd>npm rebuild node-sass</kbd>  
> 指定端口启动<kbd>npm run serve -- --port 8080</kbd>，默认端口为8080

![vue_login](image4md/vue_login.png)  
通过超级管理员账号登录，出现以下页面  
![vue_book](image4md/vue_book.png)  
恭喜您，本项目成功运行起来了！
## 3. 项目部署

### 3.1 华为云ECS部署
    当前ECS镜像为Huawei Cloud EulerOS 2.0 标准版 64位

#### 3.1.1 获取项目代码
* 通过Git 获取
```shell
    # 安装 Git
    yum install -y git
    # 进入指定路径，存放项目文件，如 /home 路径
    cd /home
    # 获取项目
    git clone git@gitee.com:HuaweiCloudDeveloper/huaweicloud-python-quick-start-samples.git
```
* 通过华为云的CloudShell将Windows下载的项目文件上传到ECS中，如 /home 路径  
![cloud_shell](image4md/cloud_shell.png)
![upload_file](image4md/upload_file.png)
> 注：当前项目文件路径为 /home/python/LibraryManagement

#### 3.1.2 进入后端根路径
    cd /home/python/LibraryManagement/flask_backend
若安装超时，可替换pip的镜像源，pip配置文件路径 /~/.pip/pip.conf（不存在可自行创建），文件内容如下
```ini
# 华为PyPI镜像
[global]  
index-url = http://mirrors.huaweicloud.com/repository/pypi/simple  
trusted-host = mirrors.huaweicloud.com  
timeout = 120
```

#### 3.1.3 安装依赖包
    pip install -r requirements-prod.txt

#### 3.1.4 数据库配置
配置 LibraryManagement/flask_backend/.production.env 文件中的 SQLALCHEMY_DATABASE_URI 选项
```ini
# MySQL数据库配置示例:  
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://username:password@ip_address:port/db_name' 
```
```ini
# PostgrepSQL数据库配置示例： 
SQLALCHEMY_DATABASE_URI = 'postgresql://username:password@ip_address:port/db_name'
```

#### 3.1.5 生产环境配置
配置文件 LibraryManagement/flask_backend/.flaskenv
```ini
FLASK_ENV=production
```

#### 3.1.6 gunicorn托管flask服务
配置文件 LibraryManagement/flask_backend/gunicorn.conf.py
```ini
# 监听外网端口5000，可外网访问 （如127.0.0.1:5000表示监听内网端口5000，只能内网访问）
bind = "0.0.0.0:5000" 
```

#### 3.1.7 数据库初始化
    # 初始化命令
    flask db init 
    # 生成图书示例数据命令
    flask db fake

#### 3.1.8 启动后端服务
    # 启动命令
    gunicorn -c gunicorn.conf.py starter:app  
![gunicorn_run](image4md/gunicorn_run.png)  
后端服务启动成功

#### 3.1.9 进入前端根路径
    cd /home/python/LibraryManagement/vue_frontend


#### 3.1.10 安装NodeJS
```shell
# 下载nodejs安装包
wget https://nodejs.org/dist/v16.17.0/node-v16.17.0-darwin-x64.tar.gz
# 解压
tar xf node-v16.17.0-linux-x64.tar.xz
# 可以删除压缩包
rm -rf node-v16.17.0-linux-x64.tar.xz
# 创建目录
mkdir /usr/local/lib/node
# 移动目录并重命名
mv node-v16.17.0-linux-x64 /usr/local/lib/node/nodejs
# 编辑文件/etc/profile，在文件底部添加环境变量
echo -e "export NODEJS_HOME=/usr/local/lib/node/nodejs\nexport PATH=$NODEJS_HOME/bin:$PATH" >> /etc/profile
# 刷新修改
source /etc/profile
# 查看版本
node -v
```

#### 3.1.11 安装依赖包
    npm install 


#### 3.1.12 配置API连接文件
配置文件 LibraryManagement/vue_frontend/.env.production
```ini
# 配置为后端服务对应URL
VUE_APP_BASE_URL = 'http://{ECS弹性公网IP}:5000/'
```

#### 3.1.13 构建前端代码
    # 构建命令，会生成dist目录
    npm run build

#### 3.1.14 安装nginx
    yum install -y nginx

#### 3.1.15 配置nginx
将 3.1.12 生成的dist目录的绝对路径替换到nginx配置文件中<kbd>/etc/nginx/nginx.conf</kbd>,  
并在配置文件开头指定对应用户，如： user root;  
![nginx_conf](image4md/nginx_conf.png)  

```shell
# nginx 启动命令
nginx
# nginx 重载命令 重载配置文件
nginx -s reload
```

#### 3.1.16 访问服务
至此，我们的项目在ECS上部署成功啦！

### 3.2 CCE部署

#### 3.2.1 安装Docker引擎
    yum install -y docker

#### 3.2.2 容器镜像服务 SWR
* 创建组织
* 获取仓库登录命令，并在ECS上运行
```shell
docker login -u {区域项目名称}@{密文} -p {密文} swr.{区域项目名称}.myhuaweicloud.com
```  
![docker_login](image4md/docker_login.png)   
![docker_login_success](image4md/docker_login_success.png)   
出现 Login Succeeded，登录成功

#### 3.2.3 构建后端镜像
```shell
# 进入后端根路径
cd /home/python/LibraryManagement/flask_backend
# 根据Dockerfile构建构建后端镜像
docker build -t "{镜像名称}:{版本名称}" .
```
> 可通过 <kbd>docker images</kbd> 命令查看镜像是否构建成功

#### 3.2.4 构建前端镜像
构建前设定访问后端服务路由，修改文件 <kbd>LibraryManagement/vue_frontend/.env.production</kbd> 中选项
```ini
# 后续会在ELB中将后端服务配置为该URL
VUE_APP_BASE_URL = '/api/' 
```
```shell
# 前端代码被修改，则需要重新构建 
npm run build
# 进入前端根路径  
cd /home/python/LibraryManagement/vue_frontend
# 根据Dockerfile构建前端镜像
docker build -t "{镜像名称}:{版本名称}" .
```

#### 3.2.5 上传镜像
```shell
# 需要运行两次，两个镜像分别进行上传
# 使用SWR信息标记本地镜像
docker tag {镜像名称}:{版本名称} swr.{区域项目名称}.myhuaweicloud.com/{组织名称}/{镜像名称}:{版本名称}
# 示例
docker tag nginx:v1 swr.cn-east-3.myhuaweicloud.com/cloud-develop/nginx:v1

# 上传镜像
docker push swr.{区域项目名称}.myhuaweicloud.com/{组织名称}/{镜像名称}:{版本名称}
# 示例
docker push swr.cn-east-3.myhuaweicloud.com/cloud-develop/nginx:v1
```
终端显示如下信息，表明上传镜像成功  
![docker_images](image4md/docker_images.png)  
返回容器镜像服务控制台，在“我的镜像”页面，执行刷新操作后可查看到对应的镜像信息

#### 3.2.6 云容器引擎 CCE
* 购买集群
* 创建节点（当前节点配置 4vCPUs | 8GiB）
* 创建前端工作负载
基本信息  
![deploy_fronted_0](image4md/deploy_fronted_0.png)    
服务配置  
![deploy_fronted_1](image4md/deploy_fronted_1.png)  
* 创建后端工作负载  
基本信息  
![deploy_backend_0](image4md/deploy_backend_0.png)  
容器环境变量  
![deploy_backend_1](image4md/deploy_backend_1.png)  
服务配置  
![deploy_backend_2](image4md/deploy_backend_2.png)  
* 配置路由 Ingress  
![ingress](image4md/ingress.png)  
![ingress_conf](image4md/ingress_conf.png)  

#### 3.2.6 访问服务
此时即可通过 http://{ELB弹性公网IP}/ 进行访问

## 4. 常见问题
* nginx托管后，访问服务报403错误，如下图  
![nginx_403](image4md/nginx_403.png)  
> 原因1：权限配置不正确，为保证文件能正确执行，nginx既需要文件的读权限，又需要文件所有父目录的可执行权限  
解决方法：将权限修改为root，在nginx的nginx.conf文件的顶部使用 user root，指定操作用户为root  
原因2：目录索引设置错误，如运行PHP网站，nginx配置文件中 index index.html index.htm index.php，当访问该网站，nginx会按照以上index配置的先后顺序在根目录查找文件，都不存在则返回403  
解决方法：确保配置的目录正确无误
* 服务启动后，内网能访问，而公网IP不能访问
>原因：安全组配置错误  
解决方法：配置弹性云主机ECS安全组，打开启动服务的端口  
* CCE集群后台实例启动未就绪，无法连接数据库  
> 原因：数据库安全组未配置  
解决方法：根据集群中的容器网段配置云数据库安全组的入方向规则  

![cce_subnet](image4md/cce_subnet.png)  
![cce_sg](image4md/cce_sg.png)  
* 使用客户端上传镜像失败, [解决方案](https://support.huaweicloud.com/swr_faq/swr_faq_0006.html)

# Github开源项目参考
* [前端vue源码参考链接](https://github.com/TaleLin/lin-cms-vue)
* [后端flask源码参考链接](https://github.com/TaleLin/lin-cms-flask)
