"""
    :copyright: © 2021 by the Lin team.
    :license: MIT, see LICENSE for more details.
"""

from typing import Dict

from lin import SpecTree
from spectree import SecurityScheme

api = SpecTree(
    backend_name="flask",
    title="Lin-CMS-Flask",
    mode="strict",
    version="0.4.0",
    # OpenAPI对所有接口描述默认返回一个参数错误, http_status_code为400。
    validation_error_status=400,
    annotations=True,
    security_schemes=[
        SecurityScheme(
            name="AuthorizationBearer",
            data={
                "type": "http",
                "scheme": "bearer",
            },
        ),
    ],
)

AuthorizationBearerSecurity: Dict = {"AuthorizationBearer": []}
