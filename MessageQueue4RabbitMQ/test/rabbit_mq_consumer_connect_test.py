import json

from MessageQueue4RabbitMQ.src.rabbit_mq_connect import RabbitMQConsumer


def call_back(channel, method, properties, body):
    msg = json.loads(body)
    print(msg)


if __name__ == '__main__':
    import sys
    consumer_type = sys.argv[1]
    num = sys.argv[2]
    if consumer_type == "direct":
        consumer = RabbitMQConsumer()
        print("消费者进程{0}开始运行".format(num))
        consumer.consume_msg("test", call_back)
        print("结束监控队列")
    elif consumer_type == "fanout":
        queue_name = "test{0}".format(num)
        consumer = RabbitMQConsumer()
        consumer.state_queue(queue_name)
        consumer.state_exchange("test_exchange", exchange_type='fanout')
        consumer.queue_bind_exchange(queue_name, "test_exchange")
        print("消费者进程{0}开始运行".format(num))
        consumer.consume_msg(queue_name, call_back)
