import unittest

from MessageQueue4RabbitMQ.src.rabbit_mq_connect import RabbitMQProducer


class RabbitMQConnectTest(unittest.TestCase):
    produce = None
    queue_name = "test"

    @classmethod
    def setUpClass(cls) -> None:
        cls.produce = RabbitMQProducer()

    @classmethod
    def tearDownClass(cls) -> None:
        cls.produce.close()

    def test_0_send_msg_to_queue_direct(self):
        """测试direct模式发送消息"""
        self.produce.state_queue(self.queue_name)
        for i in range(4):
            self.produce.send_message_to_queue("", self.queue_name, "test{0}".format(i))

    def test_1_send_msg_to_queue_fanout(self):
        """测试fanout模式发送消息"""
        exchange_name = "test_exchange"
        self.produce.state_exchange(exchange_name, exchange_type='fanout')
        for i in range(4):
            self.produce.send_message_to_queue(exchange_name, "", "test{0}".format(i))

    def test_2_delete_queue(self):
        """测试删除队列"""
        queue_name = "test2"
        self.produce.delete_queue(queue_name)

    def test_3_delete_exchange(self):
        """测试删除交换机"""
        exchange = "test_exchange"
        self.produce.delete_exchange(exchange)


if __name__ == '__main__':
    unittest.main()
