#!/usr/bin/env python
#-*- coding:utf-8 -*-

from setuptools import setup
import os
black_list = [".cid", ".codecheck", ".codelabs", ".git", ".idea", "README.md", "setup.py", "LICENSE"]
b = os.listdir(os.path.dirname(__file__))
dir_list = [i for i in b if i not in black_list]

setup(
    name='HuaweiCloud for Python',
    version='1.0',
    description='This is a test of the setup',
    packages=dir_list)
