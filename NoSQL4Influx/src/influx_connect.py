import json
import os

from influxdb import InfluxDBClient


class InfluxConnect:

    def __new__(cls, *args, **kwargs):
        # 获取配置文件路径
        dir_path = os.path.split(os.path.split(__file__)[0])[0]
        config_path = os.path.join(dir_path, f"config{os.sep}config.json")
        with open(config_path, "r") as config_file:
            # 反序列化配置文件
            config = json.load(config_file)
        config = config.get("influxdb", None)
        if not config:
            raise Exception("连接地址不存在或为空")
        return InfluxDBClient(**config)
