import unittest

from NoSQL4Influx.src.influx_connect import InfluxConnect


class InfluxConnectTest(unittest.TestCase):
    ic = None
    database = "test"

    @classmethod
    def setUpClass(cls) -> None:
        cls.ic = InfluxConnect()

    @classmethod
    def tearDownClass(cls) -> None:
        cls.ic.close()

    def test_0_create_database(self):
        """创建database"""
        self.ic.create_database(self.database)
        self.assertTrue(self.database in [i["name"] for i in self.ic.get_list_database()])

    def test_1_insert_point(self):
        """插入一个POINT"""
        point = [{
            "measurement": "test",
            "tags": {
                "id": 1,
            },
            "fields": {
                "name": "test",
                "age": 10
            }
        }]
        self.ic.switch_database(self.database)
        self.ic.write_points(point)
        result = self.ic.query(f"SELECT * FROM test WHERE id='1'", database=self.database)
        self.assertTrue(result)

    def test_3_delete_series(self):
        """删除series"""
        self.ic.delete_series(database=self.database, measurement="test", tags={"id": "1"})
        result = self.ic.query(f"SELECT * FROM test WHERE id='1'", database=self.database)
        self.assertTrue(not result)

    def test_4_drop_measurement(self):
        """删除measurement"""
        self.ic.switch_database(self.database)
        self.ic.drop_measurement(measurement="test")
        self.assertTrue("test" not in [i["name"] for i in self.ic.get_list_measurements()])

    def test_5_drop_database(self):
        """删除database"""
        self.ic.drop_database(self.database)
        self.assertTrue(self.database not in [i["name"] for i in self.ic.get_list_database()])


if __name__ == '__main__':
    unittest.main()