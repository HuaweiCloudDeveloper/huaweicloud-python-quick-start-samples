#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Copyright : (c) Copyright 2022, SDC.
@File      : mongodb_test.py
@Author    : linhaoyang
@Date      : 2022/9/19 14:24
@Desc      : 
"""
import unittest

from NoSQL4Mongo.src.mongodb import MongoConnect


class MongoDbTest(unittest.TestCase):
    mc = None

    @classmethod
    def setUpClass(cls) -> None:
        cls.mc = MongoConnect()

    @classmethod
    def tearDownClass(cls) -> None:
        cls.mc.close()

    def test_0_insert_value(self):
        """测试添加值"""
        print("开始添加值")
        # 创建database及collection
        collection = self.mc.test.test
        collection.insert_one({"id": "test"})
        if collection.find_one({"id": "test"}):
            self.assertTrue(True, "数据插入成功")
        else:
            self.assertTrue(False, "数据插入失败")

    def test_1_update_value(self):
        """测试更新值"""
        print("开始更新值")
        collection = self.mc.test.test
        collection.update_one({"id": "test"}, {"$set": {"id": "test2"}})
        if collection.find_one({"id": "test2"}):
            self.assertTrue(True, "数据更新成功")
        else:
            self.assertTrue(False, "数据更新失败")

    def test_2_delete_value(self):
        """测试删除值"""
        print("开始删除值")
        collection = self.mc.test.test
        collection.delete_many({"id": "test2"})
        if not collection.find_one({"id": "test2"}):
            self.assertTrue(True, "数据删除成功")
        else:
            self.assertTrue(False, "数据删除失败")


if __name__ == '__main__':
    unittest.main()