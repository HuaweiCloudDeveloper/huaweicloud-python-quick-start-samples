#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Copyright : (c) Copyright 2022, SDC.
@File      : mongodb.py
@Author    : linhaoyang
@Date      : 2022/9/19 11:20
@Desc      : 
"""
import json
import os

import pymongo


class MongoConnect:

    def __new__(cls, *args, **kwargs):
        # 获取配置文件路径
        dir_path = os.path.split(os.path.split(__file__)[0])[0]
        config_path = os.path.join(dir_path, r"config\config.json")
        with open(config_path, "r") as config_file:
            # 反序列化配置文件
            config = json.load(config_file)
        mongo_address = config.get("mongodb", None)
        if not mongo_address:
            raise Exception("连接地址不存在或为空")
        return pymongo.MongoClient(mongo_address)
