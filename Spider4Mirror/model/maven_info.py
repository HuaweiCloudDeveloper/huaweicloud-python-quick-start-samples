from sqlalchemy import String, Column, Integer, UniqueConstraint
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class MavenInfo(Base):

    __tablename__ = "maven_info"
    id = Column(Integer, primary_key=True, autoincrement=True)
    group_id = Column(String(255))
    artifact_id = Column(String(255))
    version = Column(String(255))
    __table_args__ = (
        # 联合唯一索引
        UniqueConstraint("group_id", "artifact_id", "version", name='idx_group_id_artifact_id_version'),
    )
