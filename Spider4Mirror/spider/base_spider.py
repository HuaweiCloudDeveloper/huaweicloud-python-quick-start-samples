from requests import Session

from Spider4Mirror.util.logger import Logger
from Spider4Mirror.util.mysql_util import MysqlUtil

USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)" \
             " Chrome/105.0.0.0 Safari/537.36"


class BaseSpider:
    """
    爬虫基类
    """
    def __init__(self):
        self.session = Session()
        self.session.headers["User-Agent"] = USER_AGENT
        self.mysql_util = MysqlUtil()
        self.log = Logger().logger

    def get_url(self, url, **kwargs):
        try:
            rep = self.session.get(url, **kwargs)
        except Exception as e:
            self.log.error("访问出错了{0}".format(e))
            raise e
        return rep
