# 华为云 Spider For Maven Mirror 演示

## 1. 说明

本项目将演示通过requests和xpath的使用，对maven镜像仓网址页面进行数据采集。采集的对象为三方包的标签。
> maven是java常用的三方包管理工具，类似于python的pypi。  
> maven镜像仓网址：https://repo1.maven.org/maven2/。  
> maven对三方包标签通常由三个元素组成，分别是group_id,artifact_id,version。

* group_id

![group_id](./image4md/group_id.PNG)

* artifact_id

![artifact_id](./image4md/artifact_id.PNG)

* version

![version](./image4md/version.PNG)

### 1.1 环境准备

* python3.9.2
* Pycharm 2022+ / Visual Studio 2022+
* SQLServer 2012+

### 1.2 必备PyPI包

* lxml==4.9.1
* sqlalchemy==1.4.41
* requests==2.28.1
* pymysql==1.0.2

### 1.3 数据库选型

* RDS For MySQL(推荐)  
  更多详情，请参考 [RDS For MySQL 成长地图](https://support.huaweicloud.com/rds/index.html#tabpane_06314173)
* 高性能云数据库 GaussDB(for MySQL)  
  更多详情，请参考 [GaussDB(For MySQL) 成长地图](https://support.huaweicloud.com/gaussdb/index.html)

### 1.4 核心代码介绍

* spider\base_spider.py  
  此类为采集基类，使用requests包的Session对象，将一些通用的访问方法进行封装。后续可根据不同网站继承该基类进行采集方式的拓展。

```python
class BaseSpider:
    """
    爬虫基类
    """

    def __init__(self):
        self.session = Session()
        self.session.headers["User-Agent"] = USER_AGENT
        self.mysql_util = MysqlUtil()
        self.log = Logger().logger

    def get_url(self, url, **kwargs):
        try:
            rep = self.session.get(url, **kwargs)
        except Exception as e:
            self.log.error("访问出错了{0}".format(e))
            raise e
        return rep
```

* spider\maven_mirror_spider.py  
  MavenMirrorSpider继承自BaseSpider，类方法实现了对Maven镜像仓的采集规则。

```python
class MavenMirrorSpider(BaseSpider):

    def __init__(self, limit=10):

    def find_all_node(self, this_level_url, next_level_text):

    @classmethod
    def concatenate_next_level_url(cls, this_level_url, next_level_text):

    def artifact_id_level_loop(self, group_id):

    @classmethod
    def create_info_obj(cls, group_id, artifact_id, version):

    def version_level_loop(self, second_url, group_id, artifact_id):

    def run(self):

```

* model\maven_info.py  
  MavenInfo是表名为maven_info数据表模型

```python
class MavenInfo(Base):
    __tablename__ = "maven_info"
    id = Column(Integer, primary_key=True, autoincrement=True)
    group_id = Column(String(255))
    artifact_id = Column(String(255))
    version = Column(String(255))
    __table_args__ = (
        # 联合唯一索引
        UniqueConstraint("group_id", "artifact_id", "version", name='idx_group_id_artifact_id_version'),
    )

```

* util\mysql_util.py  
  MySqlUtil封装了一些通用的数据库操作，包括了数据库连接以及数据增查。

## 2.快速开始

### 2.1 必备程序

1. window10操作系统
2. 下载并安装[python3.9.2](https://www.python.org/downloads/release/python-392/) 。(注意安装后需要确认是否设置了python环境变量)
3. 下载并安装[Pycharm 2022+](https://www.jetbrains.com.cn/en-us/pycharm/)
4. 下载并安装搭建本地[MySQL数据库](https://www.mysql.com/)   
   或通过购买华为云[云数据库 RDS For MySQL](https://support.huaweicloud.com/rds/index.html)
   绑定[弹性公网IP](https://support.huaweicloud.com/eip/index.html) ，并开启安全组3306端口

![安全组设置](image4md/security_group_settings.PNG)

### 2.2 运行环境搭建

1. 代码克隆  
   方式1 git命令
   > cd 指定代码存放文件夹  
   > git clone git@gitee.com:HuaweiCloudDeveloper/huaweicloud-python-quick-start-samples.git
   
2. 安装相关第三方Pypi包

> cd 代码目录/Spider4Mirror/  
> pip install -r requirements.txt

3. 修改配置文件

* 修改 代码目录/Spider4Mirror/config/config.json

```json
{
  "mysql": {
    "host": "ip_address",
    "port": "port",
    "user": "username",
    "password": "password",
    "database": "db_name"
  }
}
```

> ip_address: 数据库IP地址  
> port: 数据库端口  
> username: 用户名   
> password: 用户密码   
> db_name: 数据库名（注意：首次安装MySQL需要运行SQL创建数据库，SQL语句：CREATE DATABASE db_name;）

4. 运行

* 命令行运行

> cd 代码目录/Spider4Mirror/spider  
> python maven_mirror_spider.py

* pycharm测试运行

![pycharm运行](image4md/pycharm_test.PNG)

## 3.项目部署

### 3.1 基础环境搭建

#### 方式1 Huawei Cloud EulerOS部署

1. 创建虚拟私有云 VPC ([快速入门](https://support.huaweicloud.com/qs-vpc/vpc_qs_0001.html))
2. 购买弹性云服务器 ECS ([快速入门](https://support.huaweicloud.com/qs-ecs/zh-cn_topic_0030831985.html))
   ，实例绑定VPC，操作系统选装Huawei Cloud EulerOS。
3. 购买云数据库 RDS For MySQL([快速入门](https://support.huaweicloud.com/qs-rds/zh-cn_topic_0046585334.html)) ，实例绑定VPC。
4. 购买弹性公网IP EIP([快速入门](https://support.huaweicloud.com/qs-eip/eip_qs_0002.html)) ，云服务器实例绑定弹性公网IP。  
   注意在购买云服务器实例时，可购买并绑定弹性公网IP。

#### 方式2 docker部署

1. 创建虚拟私有云 VPC ([快速入门](https://support.huaweicloud.com/qs-vpc/vpc_qs_0001.html))
2. 购买弹性云服务器 ECS ([快速入门](https://support.huaweicloud.com/qs-ecs/zh-cn_topic_0030831985.html))
   ，实例绑定VPC，操作系统选装Huawei Cloud EulerOS。
3. 购买云数据库 RDS For MySQL([快速入门](https://support.huaweicloud.com/qs-rds/zh-cn_topic_0046585334.html)) ，实例绑定VPC。
4. 创建云容器实例 CCI命名空间([快速入门](https://support.huaweicloud.com/qs-cci/cci_qs_0001.html))
5. 购买公网NAT网关([快速入门](https://support.huaweicloud.com/qs-natgateway/zh-cn_topic_0087895790.html))
   并绑定虚拟私有云 VPC及弹性公网IP EIP， 配置SNAT规则
6. 创建容器镜像服务 SWR组织([快速入门](https://support.huaweicloud.com/qs-swr/index.html))
   ![docker login](image4md/create_organization.PNG)

### 3.2 部署方式

#### 方式1 Huawei Cloud EulerOS部署

1. 远程登录弹性云主机ECS

2. 代码克隆

> sudo yum install -y git
> cd /home  
> git clone git@gitee.com:HuaweiCloudDeveloper/huaweicloud-python-quick-start-samples.git

3. 安装第三方Pypi包

> cd /home/Python/Spider4Mirror
> pip install -r requirements.txt

4. 配置环境变量

> export MYSQL_ADDRESS='mysql+pymysql://user:password@host:port/database'

5. 运行

> python /home/Python/Spider4Mirror/spider/maven_mirror_spider.py

#### 方式2 docker部署

1. 远程登录弹性云主机ECS

2. 代码克隆

> sudo yum install -y git
> cd /home  
> git clone git@gitee.com:HuaweiCloudDeveloper/huaweicloud-python-quick-start-samples.git

3. 安装docker

> sudo yum install -y docker

4. docker登录SWR

> docker login -u 区域@密文 -p 密文 swr.区域.myhuaweicloud.com    
![docker login](image4md/docker_login.PNG)

5. 进入项目目录

> cd /home/Python/Spider4Mirror

6. 构建镜像

> docker build -t 镜像名:版本 .

7. 镜像打标

> docker tag 镜像名:版本 swr.区域.myhuaweicloud.com/组织名/镜像名:版本

8. 镜像推送

> docker push swr.区域.myhuaweicloud.com/组织名/镜像名:版本

9. 镜像运行（以任务(job)工作负载类型为例）

![cci_01](image4md/CCI_01.PNG)

步骤1 选择工作负载类型（此容器适合使用任务(job)或定时任务(cron)）  
步骤2 选择命名空间  
步骤3 镜像创建

![cci_02](image4md/CCI_02.PNG)

步骤1 任务名称重命名（可选）  
步骤2 选择Pod规格  
步骤3 搜索镜像

![cci_03](image4md/CCI_03.PNG)

步骤1 选择镜像版本  
步骤2 输入容器环境变量:PYTHON = /app   
步骤3 输入容器环境变量:MYSQL_ADDRESS = mysql+pymysql://user:password@host:port/database  
步骤4 点击下一步，直到成功创建容器实例

### 3.3 常见问题

1. VPC配置NAT网关的SNAT规则，可以让配置的子网段的所有服务实例可以访问公网。安装pypi包等一系列操作是需要访问公网的。

2. 数据库需要开通安全组的访问端口，不然ECS或CCI的实例无法访问数据库。
   ![security_group_settings_01](image4md/security_group_settings_01.PNG)
   0.0.0.0/0指所有IP（包括VPC下的子网IP和公网所有IP）都可以访问。  
   公网IP要访问该数据库实例，数据库实例需要绑定弹性公网IP或配置NAT网关的DNAT规则。
