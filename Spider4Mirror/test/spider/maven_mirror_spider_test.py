import unittest

from Spider4Mirror.model.maven_info import MavenInfo
from Spider4Mirror.spider.maven_mirror_spider import MavenMirrorSpider, MAVEN_MIRROR_BASE_URL


class MavenMirrorSpiderTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.maven_mirror_spider = MavenMirrorSpider()

    def test_1_find_all_node(self):
        """测试查找所有节点"""
        fist_level = self.maven_mirror_spider.find_all_node(MAVEN_MIRROR_BASE_URL, "")
        self.assertTrue(fist_level)
        node = fist_level[1]
        second_level = self.maven_mirror_spider.find_all_node(MAVEN_MIRROR_BASE_URL, node)
        self.assertTrue(second_level)
        second_url = "{0}{1}".format(MAVEN_MIRROR_BASE_URL, node)
        third_level = self.maven_mirror_spider.find_all_node(second_url, second_level[1])
        self.assertTrue(third_level)

    def test_2_run(self):
        """测试运行"""
        self.maven_mirror_spider.run()
        result = self.maven_mirror_spider.mysql_util.query_one(MavenInfo, [], [])
        self.assertTrue(result)


if __name__ == '__main__':
    unittest.main()