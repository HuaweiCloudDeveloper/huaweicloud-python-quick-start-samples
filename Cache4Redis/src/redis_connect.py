#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Copyright : (c) Copyright 2022, SDC.
@File      : redis_connect.py
@Author    : linhaoyang
@Date      : 2022/9/19 14:24
@Desc      :
"""
import json
import os

import redis


class RedisConnect:

    def __new__(cls, *args, **kwargs):
        # 获取配置文件路径
        dir_path = os.path.split(os.path.split(__file__)[0])[0]
        config_path = os.path.join(dir_path, f"config{os.sep}config.json")
        with open(config_path, "r") as config_file:
            # 反序列化配置文件
            config = json.load(config_file).get("redis", None)
        if not config:
            raise Exception("连接配置不存在或为空")
        # redis读取的值默认为二进制形式, decode_responses对查询的值进行解码
        return redis.Redis(**config, decode_responses=True)
