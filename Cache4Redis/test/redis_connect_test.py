#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Copyright : (c) Copyright 2022, SDC.
@File      : redis_connect_test.py
@Author    : linhaoyang
@Date      : 2022/9/19 14:24
@Desc      :
"""
import unittest

from Cache4Redis.src.redis_connect import RedisConnect


class RedisConnectTest(unittest.TestCase):

    redis_connect = None

    @classmethod
    def setUpClass(cls) -> None:
        cls.redis_connect = RedisConnect()

    @classmethod
    def tearDownClass(cls) -> None:
        cls.redis_connect.close()

    def test_0_insert_value(self):
        """测试添加值"""
        print("开始添加值")
        self.redis_connect.set("test", "test")
        result = self.redis_connect.get("test")
        self.assertTrue(result == "test")

    def test_1_delete_value(self):
        """测试删除值"""
        print("开始删除值")
        self.redis_connect.delete("test")
        result = self.redis_connect.get("test")
        self.assertTrue(result is None)


if __name__ == '__main__':
    unittest.main()